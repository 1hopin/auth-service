FROM golang:1.18-alpine
RUN mkdir /app
# ADD ../ /app/
WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download
RUN go mod verify

COPY . ./

# RUN go get -v github.com/cosmtrek/air
# ENTRYPOINT ["air", "go", "run", "main.go"]
ENTRYPOINT ["go", "run", "main.go"]