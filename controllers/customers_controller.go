package controllers

import (
	"customer-service/models/requests"
	"customer-service/models/responses"
	util "customer-service/utils"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator"
	"gitlab.com/1hopin/go-module/constants"
	"gitlab.com/1hopin/go-module/utils"
)

type CustomerController interface {
	SignIn(c *gin.Context)
}

type customerController struct {
}

func InitCustomerController() CustomerController {
	return &customerController{}
}

func (controller *customerController) SignIn(c *gin.Context) {
	var req requests.Customer
	c.Bind(&req)

	if err := validator.New().Struct(req); err != nil {
		utils.RespondWithJSON(c, http.StatusBadRequest, constants.InvalidRequest, err.Error(), nil)
		return
	}

	token := util.GenToken(req.Id)

	res := responses.JWT{
		Token: token,
	}

	utils.RespondWithJSON(c, http.StatusOK, constants.Success, constants.Success, res)
}
