package requests

type Customer struct {
	Id int64 `json:"id" validate:"required"`
}
