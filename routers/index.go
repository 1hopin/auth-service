package routers

import (
	"customer-service/controllers"
	"net/http"

	"github.com/gin-gonic/gin"
)

func SetupRouter(router *gin.Engine) {

	customerCon := controllers.InitCustomerController()

	customerV1 := router.Group("/authen/v1/")
	{
		customerV1.POST("signin", customerCon.SignIn)
	}

	router.GET("/", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{
			"message": "Auth Service is running.",
		})
	})
}
