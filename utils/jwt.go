package util

import (
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
)

func GenToken(id int64) string {
	claims := &jwt.MapClaims{
		"exp": time.Now().Add(5 * time.Minute).Unix(), //1 Min
		"id":  id,
	}

	JwtToken := jwt.NewWithClaims(
		jwt.SigningMethodHS256,
		claims)

	token, _ := JwtToken.SignedString([]byte(os.Getenv("JWT_SECRET")))
	// if err != nil {
	// 	c.JSON(http.StatusInternalServerError, gin.H{
	// 		"error": err.Error(),
	// 	})
	// }

	return token
}
